﻿ARG BUILD_VERSION
ARG UNITY_VERSION
FROM gableroux/unity3d:${UNITY_VERSION}${BUILD_VERSION} AS unity-env

RUN apt-get update && \
    apt-get install -y software-properties-common && \
    add-apt-repository ppa:thomas-schiex/blender && \
    apt-get install -y blender zip
    
FROM gableroux/unity3d:${UNITY_VERSION}${BUILD_VERSION} AS unity-env-webgl

RUN apt-get update && \
    apt-get install -y ffmpeg software-properties-common && \
    add-apt-repository ppa:thomas-schiex/blender && \
    apt-get update && \
    apt-get install -y blender zip