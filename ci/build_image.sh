﻿#!/usr/bin/env bash

docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

docker build --build-arg UNITY_VERSION=$UNITY_VERSION \
             --build-arg BUILD_VERSION=$BUILD_VERSION \
             --target $UNITY_ENV \
             --cache-from $CI_REGISTRY/seredos/unitytetris3dexample/unity$BUILD_VERSION:$IMAGE_TAG \
             -t $CI_REGISTRY/seredos/unitytetris3dexample/unity$BUILD_VERSION:$IMAGE_TAG .
docker push $CI_REGISTRY/seredos/unitytetris3dexample/unity$BUILD_VERSION:$IMAGE_TAG

## build an push windows image
#docker build --build-arg UNITY_VERSION=$UNITY_VERSION \
#             --build-arg BUILD_VERSION=-windows \
#             --target unity-env \
#             --cache-from $CI_REGISTRY/seredos/unitytetris3dexample/unity-windows:$IMAGE_TAG \
#             -t $CI_REGISTRY/seredos/unitytetris3dexample/unity-windows:$IMAGE_TAG .
#docker push $CI_REGISTRY/seredos/unitytetris3dexample/unity-windows:$IMAGE_TAG
#
## build an push linux image
#docker build --build-arg UNITY_VERSION=$UNITY_VERSION \
#             --build-arg BUILD_VERSION= \
#             --target unity-env \
#             --cache-from $CI_REGISTRY/seredos/unitytetris3dexample/unity-linux:$IMAGE_TAG \
#             -t $CI_REGISTRY/seredos/unitytetris3dexample/unity-linux:$IMAGE_TAG .
#docker push $CI_REGISTRY/seredos/unitytetris3dexample/unity-linux:$IMAGE_TAG
#
## build an push WebGL image
#docker build --build-arg UNITY_VERSION=$UNITY_VERSION \
#             --build-arg BUILD_VERSION=-webgl \
#             --target unity-env-webgl \
#             --cache-from $CI_REGISTRY/seredos/unitytetris3dexample/unity-webgl:$IMAGE_TAG \
#             -t $CI_REGISTRY/seredos/unitytetris3dexample/unity-webgl:$IMAGE_TAG .
#docker push $CI_REGISTRY/seredos/unitytetris3dexample/unity-webgl:$IMAGE_TAG
#
## build an push Android image
#docker build --build-arg UNITY_VERSION=$UNITY_VERSION \
#             --build-arg BUILD_VERSION=-android \
#             --target unity-env-webgl \
#             --cache-from $CI_REGISTRY/seredos/unitytetris3dexample/unity-android:$IMAGE_TAG \
#             -t $CI_REGISTRY/seredos/unitytetris3dexample/unity-android:$IMAGE_TAG .
#docker push $CI_REGISTRY/seredos/unitytetris3dexample/unity-android:$IMAGE_TAG