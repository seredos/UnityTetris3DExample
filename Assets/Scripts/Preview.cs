﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Preview : MonoBehaviour
{
    public GameObject[] groups;
    public GameObject currentObject;
    
    // Start is called before the first frame update
    void Start()
    {
        currentObject = spawnNext();
    }

    // Update is called once per frame
    void Update()
    {
        if (!currentObject)
        {
            currentObject = spawnNext();
        }
    }
    
    public GameObject spawnNext()
    {
        int i = Random.Range(0, groups.Length);

        GameObject group = Instantiate(groups[i], transform.position, Quaternion.identity);
        group.transform.parent = transform;
        return group;
    }
}
