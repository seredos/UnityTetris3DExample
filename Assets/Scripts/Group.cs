﻿using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;

public class Group : MonoBehaviour
{
    public bool active = false;
    public string type;
    private float last = 0;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (active)
        {
            GameInput inputClass = FindObjectOfType<GameInput>();
            bool fall = false;

            if (inputClass.isMoveLeft())
            {
                move(-1);
            }else if (inputClass.isMoveRight())
            {
                move(1);
            }else if (inputClass.isRotateLeft() && type != "O")
            {
                rotate(-90);
            }else if (inputClass.isRotateRight() && type != "O")
            {
                rotate(90);
            }else if (inputClass.isMoveDown())
            {
                fall = true;
            }
            
            if (fall || Time.time - last >= 1)
            {
                transform.position += new Vector3(0, -1, 0);
                if (FindObjectOfType<Playfield>().isValidGridPos(transform))
                {
                    updateGrid();
                }
                else
                {
                    transform.position += new Vector3(0, 1, 0);
                    active = false;
                    inputClass.clearFall();
                    FindObjectOfType<Playfield>().deleteFullRows();
                    FindObjectOfType<Spawner>().currentObject = null;
                }
                last = Time.time;
            }

            //transform.Translate(Vector3.down * Time.deltaTime, Space.World);
        }
    }

    private void move(float x)
    {
        transform.position += new Vector3(x,0,0);
        if (FindObjectOfType<Playfield>().isValidGridPos(transform))
        {
            updateGrid();
        }
        else
        {
            transform.position += new Vector3(x*-1,0,0);            
        }
    }

    private void rotate(float angle)
    {
        transform.Rotate(0,0,angle);
        if (FindObjectOfType<Playfield>().isValidGridPos(transform))
        {
            updateGrid();
        }
        else
        {
            transform.Rotate(0,0,angle*-1);
        }
    }
    
    public void activate()
    {
        active = true;
    }

    void updateGrid()
    {
        for (int y = 0; y < Playfield.height; y++)
        {
            for (int x = 0; x < Playfield.width; x++)
            {
                if (Playfield.grid[x, y] != null)
                {
                    if (Playfield.grid[x, y].parent == transform)
                    {
                        Playfield.grid[x, y] = null;
                    }
                }
            }
        }

        foreach (Transform child in transform)
        {
            Vector2 vec = FindObjectOfType<Playfield>().calculateGridVector(child.position);
            Playfield.grid[(int)vec.x, (int)vec.y] = child;
        }
    }
}
