﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameInput : MonoBehaviour
{
    public bool rotateLeft = false;

    public bool rotateRight = false;

    public bool moveLeft = false;

    public bool moveRight = false;

    public bool moveDown = false;

    // Update is called once per frame
    void Update()
    {
        moveDown = GetKeyVal(KeyCode.S, moveDown);
    }

    public bool isMoveDown()
    {
        return moveDown;
    }
    
    public bool isMoveLeft()
    {
        return GetKeyVal(KeyCode.A, false);
    }

    public bool isMoveRight()
    {
        return GetKeyVal(KeyCode.D, false);        
    }

    public bool isRotateLeft()
    {
        return GetKeyVal(KeyCode.Q, false);
    }

    public bool isRotateRight()
    {
        return GetKeyVal(KeyCode.E, false);        
    }

    public void clearFall()
    {
        moveDown = false;
    }
    
    private bool GetKeyVal(KeyCode code, bool currentVal)
    {
        if (Input.GetKeyDown(code))
        {
            return true;
        }else if (Input.GetKeyUp(code))
        {
            return false;
        }

        return currentVal;
    }
}
