﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject preview;
    public GameObject currentObject;
    
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (!currentObject)
        {
            currentObject = preview.GetComponent<Preview>().currentObject;
            preview.GetComponent<Preview>().currentObject = null;
            currentObject.GetComponent<Group>().activate();
            currentObject.transform.position = transform.position;
            currentObject.transform.parent = transform;
        }
    }
}
