﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Playfield : MonoBehaviour
{
    public GameObject tile;
    public GameObject spawner;
    
    public static int width = 10;
    public static int height = 20;
    public static Transform[,] grid = new Transform[width,height];
    
    // Start is called before the first frame update
    void Start()
    {
        Vector3 spawnPos = new Vector3();
        spawnPos.y = (height / 2) -2 + transform.position.y;
        spawnPos.x = transform.position.x;
        spawner.transform.position = spawnPos;
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                Vector3 pos = transform.position;
                pos.x += (x - (width/2));
                pos.y += (y - (height/2));

                GameObject currentTile = Instantiate(tile, pos, Quaternion.identity);
                currentTile.transform.parent = this.transform;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
    }

    public Vector2 calculateGridVector(Vector3 pos)
    {
        float posX = pos.x + (width / 2) - transform.position.x;
        float posY = ((height / 2) - 1) - pos.y + transform.position.y;
        return new Vector2(Mathf.Round(posX), Mathf.Round(posY));
    }

    public bool isValidGridPos(Transform transform)
    {
        foreach (Transform child in transform)
        {
            Vector2 v = calculateGridVector(child.position);
            if (!insideBorder(v))
            {
                return false;
            }

            if (grid[(int) v.x, (int) v.y] != null && grid[(int) v.x, (int) v.y].parent != transform)
            {
                return false;
            }
        }

        return true;
    }

    private bool isRowFull(int y)
    {
        for (int x = 0; x < width; ++x)
        {
            if (grid[x, y] == null)
            {
                return false;
            }
        }

        return true;
    }

    private void deleteRow(int y)
    {
        for (int x = 0; x < width; ++x)
        {
            Destroy(grid[x,y].gameObject);
            grid[x, y] = null;
        }
    }

    private void decreaseRow(int y)
    {
        for (int x = 0; x < width; ++x)
        {
            if (grid[x, y] != null)
            {
                grid[x, y + 1] = grid[x, y];
                grid[x, y] = null;
                
                grid[x,y+1].position += new Vector3(0,-1,0);
            }
        }
    }

    private void decreateRowsAbove(int y)
    {
        for (int i = y; i > 0; --i)
        {
            decreaseRow(i);
        }
    }
    
    public void deleteFullRows()
    {
        int kills = 0;
        for (int y = 0; y < height; ++y)
        {
            if (isRowFull(y))
            {
                kills++;
                deleteRow(y);
                decreateRowsAbove(y-1);
                --y;
            }
        }
        FindObjectOfType<Score>().killLines(kills);
    }

    public static bool insideBorder(Vector2 pos)
    {
        return ((int)pos.x >= 0 && (int)pos.x < width && (int)pos.y >= 0 && (int)pos.y < height);
    }
}
